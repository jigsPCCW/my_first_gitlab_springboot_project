package com.example.swaggerSprintBoot;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.example.swaggerSprintBoot.controller.domain.CommunicationMessage;
import com.example.swaggerSprintBoot.dao.CommunicationMessageDAO;
import com.example.swaggerSprintBoot.model.CommunicationMessageModel;
import com.example.swaggerSprintBoot.service.CommunicationMessageService;

@SpringBootTest
class SwaggerSprintBootApplicationTests {

	@Autowired
	private CommunicationMessageService msgSrv;
	
	@MockBean
	private CommunicationMessageDAO msgDao;
	
	private static CommunicationMessageModel msgModel;
	
	@BeforeAll
	public static void setUp() {
		msgModel = new CommunicationMessageModel();
		msgModel.setContent("ABC");
	}
	
	@Test
	public void save() {
		when(msgDao.save(any(CommunicationMessageModel.class))).thenReturn(msgModel);
		assertEquals(msgModel,msgSrv.save(new CommunicationMessage()));
	}
	
	@Test
	public void delete() {
		msgSrv.deleteById(1L);
		when(msgDao.findById(1L)).thenReturn(null);
		assertEquals(null, msgSrv.getById(1L));
	}
}
