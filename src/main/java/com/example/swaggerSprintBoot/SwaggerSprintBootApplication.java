package com.example.swaggerSprintBoot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwaggerSprintBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(SwaggerSprintBootApplication.class, args);
	}

}
