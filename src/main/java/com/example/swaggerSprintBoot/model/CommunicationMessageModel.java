package com.example.swaggerSprintBoot.model;

import java.time.OffsetDateTime;
import java.util.Date;

import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "communicationmessage", schema ="\"test\"")
@Type(JsonBinaryType.class)
public class CommunicationMessageModel {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	  @Column(name = "content")
	  private String content;

	  @Column(name = "email")
	  private String email;

	  @Column(name = "sender")
	  private String sender;
	  
	  @Column(name = "createDate")
	  private OffsetDateTime createDate;
	  
	  @Column(name = "sysdate")
	  private Date sysDate = new Date(System.currentTimeMillis());
	  
	  @Column(name = "timeDetails", columnDefinition = "jsonb")
	  @JdbcTypeCode(SqlTypes.JSON)
	  private TimeDetailsModel td;

	public Date getSysDate() {
		return sysDate;
	}

	public void setSysDate(Date sysDate) {
		this.sysDate = sysDate;
	}

	public TimeDetailsModel getTd() {
		return td;
	}

	public void setTd(TimeDetailsModel td) {
		this.td = td;
	}

	@Override
	public String toString() {
		return "CommunicationMessageModel [id=" + id + ", content=" + content + ", email=" + email + ", sender="
				+ sender + ", createDate=" + createDate + "]";
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public OffsetDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(OffsetDateTime createDate) {
		this.createDate = createDate;
	}
	  
}
