package com.example.swaggerSprintBoot.model;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.OffsetDateTimeSerializer;
import com.vladmihalcea.hibernate.type.util.ObjectMapperWrapper.OffsetDateTimeDeserializer;

public class TimeDetailsModel {
	

	@JsonSerialize(using = OffsetDateTimeSerializer.class)
	@JsonDeserialize(using = OffsetDateTimeDeserializer.class)
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	private OffsetDateTime startTime;

	@JsonSerialize(using = OffsetDateTimeSerializer.class)
	@JsonDeserialize(using = OffsetDateTimeDeserializer.class)
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	private OffsetDateTime endTime;
	
	@Override
	public String toString() {
		return "TimeDetails [startTime=" + startTime + ", endTime=" + endTime + "]";
	}
	
	public OffsetDateTime getStartTime() {
		return startTime;
	}
	public void setStartTime(OffsetDateTime startTime) {
		this.startTime = startTime;
	}
	public OffsetDateTime getEndTime() {
		return endTime;
	}
	public void setEndTime(OffsetDateTime endTime) {
		this.endTime = endTime;
	}

}
