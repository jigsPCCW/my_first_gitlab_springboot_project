package com.example.swaggerSprintBoot.controller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.swaggerSprintBoot.controller.domain.CommunicationMessage;
import com.example.swaggerSprintBoot.model.CommunicationMessageModel;
import com.example.swaggerSprintBoot.service.CommunicationMessageService;

@RestController
@RequestMapping("/tmf-api/v4")
public class NotificationController {
	
	@Autowired
	private CommunicationMessageService msgSrv ;
	
	private final Logger log = LoggerFactory.getLogger(NotificationController.class);
	
	@PostMapping("/communicationMessage")
	public CommunicationMessageModel postSendNotificationMsg(@RequestBody CommunicationMessage processReceived){
		
		try {
			
			log.info("Insert Notification data process {}",processReceived.toString());
			return msgSrv.save(processReceived);
			/*sending SMS notification*/
			/*sending app notification*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
		return null;
	}
	
	@DeleteMapping("/deleteById/{id}")
	public String deleteById(@PathVariable Long id) {
		log.info("Inside deleteById: {}",id);
		msgSrv.deleteById(id);
		return id+" Deleted Succesfully";
	}
	
	@GetMapping("/getById/{id}")
	public ResponseEntity<CommunicationMessageModel> getMsgById(@PathVariable Long id) {
		log.info("Inside getMsgById: "+id);
		CommunicationMessageModel msg =  msgSrv.getById(id);
		if(msg == null)
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		else
			return ResponseEntity.status(HttpStatus.OK).body(msg);
	}

}