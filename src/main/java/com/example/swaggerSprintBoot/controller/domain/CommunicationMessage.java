package com.example.swaggerSprintBoot.controller.domain;

import java.time.OffsetDateTime;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.swaggerSprintBoot.model.TimeDetailsModel;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;


@JsonTypeName("comunicationMessage")
public class CommunicationMessage {
	
	  @JsonProperty("content")
	  private String content;

	  @JsonProperty("email")
	  private String email;

	  @JsonProperty("sender")
	  private String sender;
	  
	  @Autowired
	  @JsonProperty("attachment")
	  private Attachment[] attachment;
	  
	  @Override
	public String toString() {
		return "CommunicationMessage [content=" + content + ", email=" + email + ", sender=" + sender + ", attachment="
				+ Arrays.toString(attachment) + ", createDate=" + createDate + "]";
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public Attachment[] getAttachment() {
		return attachment;
	}

	public void setAttachment(Attachment[] attachment) {
		this.attachment = attachment;
	}

	public OffsetDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(OffsetDateTime createDate) {
		this.createDate = createDate;
	}

	@JsonProperty("createDate")
	  private OffsetDateTime createDate;
	
	@JsonProperty("timeDate")
	private TimeDetailsModel timeDate;

	public TimeDetailsModel getTimeDate() {
		return timeDate;
	}

	public void setTimeDate(TimeDetailsModel timeDate) {
		this.timeDate = timeDate;
	}
}
