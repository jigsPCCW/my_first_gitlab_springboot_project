package com.example.swaggerSprintBoot.controller.domain;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("TimeDetails")
public class TimeDetails {
	@Override
	public String toString() {
		return "TimeDetails [startTime=" + startTime + ", endTime=" + endTime + "]";
	}
	
	public OffsetDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(OffsetDateTime startTime) {
		this.startTime = startTime;
	}

	public OffsetDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(OffsetDateTime endTime) {
		this.endTime = endTime;
	}

	@JsonProperty("startTime")
	private OffsetDateTime startTime;
	
	@JsonProperty("endTime")
	private OffsetDateTime endTime;
}
