package com.example.swaggerSprintBoot.service;

import com.example.swaggerSprintBoot.controller.domain.CommunicationMessage;
import com.example.swaggerSprintBoot.model.CommunicationMessageModel;

public interface CommunicationMessageService {
	CommunicationMessageModel save(CommunicationMessage msg);
	void deleteById(Long id);
	CommunicationMessageModel getById(Long id);
}
