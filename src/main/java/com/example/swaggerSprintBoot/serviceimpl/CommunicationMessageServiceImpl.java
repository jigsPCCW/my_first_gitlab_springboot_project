package com.example.swaggerSprintBoot.serviceimpl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.swaggerSprintBoot.controller.domain.CommunicationMessage;
import com.example.swaggerSprintBoot.dao.CommunicationMessageDAO;
import com.example.swaggerSprintBoot.model.CommunicationMessageModel;
import com.example.swaggerSprintBoot.service.CommunicationMessageService;

@Service
public class CommunicationMessageServiceImpl implements CommunicationMessageService {

	@Autowired
	private CommunicationMessageDAO msgDao;
	
	@Override
	public CommunicationMessageModel save(CommunicationMessage msg) {
		CommunicationMessageModel msgModel = new CommunicationMessageModel();
		msgModel.setContent(msg.getContent());
		msgModel.setEmail(msg.getEmail());
		msgModel.setSender(msg.getSender());
		msgModel.setCreateDate(msg.getCreateDate());
		msgModel.setTd(msg.getTimeDate());
		return msgDao.save(msgModel);
	}

	@Override
	public void deleteById(Long id) {
		msgDao.deleteById(id);
		
	}

	@Override
	public CommunicationMessageModel getById(Long id) {
		Optional<CommunicationMessageModel> msg = msgDao.findById(id);
		if(msg!=null && msg.isPresent())
			return msg.get();
		else
			return null;
	}
	
}
