package com.example.swaggerSprintBoot.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.swaggerSprintBoot.model.CommunicationMessageModel;

@Repository
public interface CommunicationMessageDAO extends JpaRepository<CommunicationMessageModel, Long>{

}
