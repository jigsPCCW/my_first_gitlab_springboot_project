FROM openjdk:20-ea-17-jdk-slim-buster
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
COPY target/classes/ca.pem /var/jenkins_home/certs/ca.pem
ENV DOCKER_CERT_PATH=/certs/client
EXPOSE 9090
ENTRYPOINT ["java","-jar","/app.jar"]