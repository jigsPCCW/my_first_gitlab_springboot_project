pipeline {
    agent any
    tools {
    	maven 'maven'
      jdk 'jdk-17.0.6'
    }
    environment{    
	   REPONAME = "jigithapccw/myfirstrepo"
        DOCKERHUB_CREDENTIALS = credentials('JigithaDockerHub')
        CONTAINERNAME = "jigithapccw_myfirstjenImg"
    }
    triggers {
        pollSCM '* * * * *'
    }
    stages {

	  stage('Checkout') {
		steps {
			echo "Building.."
			checkout([$class: 'GitSCM', branches: [[name: '*/main']], extensions: [], userRemoteConfigs: [[url: 'https://gitlab.com/jigsPCCW/my_first_gitlab_springboot_project.git']]])
		}
	  }
        stage('Build') {
            steps {
                echo "Building.."
		    sh '''
		    mvn clean install -DskipTests
		    '''
            }
        }
        stage('Test') {
            steps {
                echo "Testing.."
                sh '''
                mvn test
                '''
            }
        }
	  stage('Scan') {
          steps {
            withSonarQubeEnv(installationName: 'sq1') { 
              sh '''
			mvn org.sonarsource.scanner.maven:sonar-maven-plugin:3.9.0.2155:sonar
		   '''
            }
          }
        }
	  stage('Docker Build & Push') {
		steps {
			echo "Docker build.."
                sh '''
				docker build -t ${REPONAME}:${BUILD_NUMBER} -f Dockerfile .
				echo $DOCKERHUB_CREDENTIALS_PSW | docker login -u $DOCKERHUB_CREDENTIALS_USR --password-stdin
				docker push ${REPONAME}:${BUILD_NUMBER}
			'''
		}
    	  }
        stage('Deploy') {
            steps {
                echo 'Deliver....'
                sh '''
		     OLD_BUILD_NUMBER=`expr $BUILD_NUMBER - 1`
		     docker stop ${CONTAINERNAME} 2> /dev/null || true
		     docker rm ${CONTAINERNAME} 2> /dev/null || true
		     docker rmi ${REPONAME}:${OLD_BUILD_NUMBER} 2> /dev/null || true
                docker run -p 9090:9090 -d --name ${CONTAINERNAME} ${REPONAME}:${BUILD_NUMBER}
                '''
            }
        }
    }
    post {
        always {
            sh 'docker logout'
        }
    }
}